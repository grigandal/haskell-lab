module TR where

data Tree a = EmptyTree | Node a (Tree a) (Tree a)
  deriving Show

list2Tree :: (Ord a) => [a] -> Tree a
list2Tree [] = EmptyTree
list2Tree xs = Node x 
    (list2Tree (take m xs_ordered)) 
    (list2Tree (drop (m+1) xs_ordered))
    where 
        m = (length xs) `div` 2
        xs_ordered = xs
        x = xs_ordered !! m
        lt_x = take m xs_ordered
        gt_x = drop (m + 1) xs_ordered