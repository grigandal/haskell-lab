# Задача о размене монет

Дано:

*Дана сумма n и список монет номиналов c1,c2,..,cm. Найти количество способов набрать сумму n используя данные номиналы (номиналы могут повторяться, либо некоторые не использоваться вовсе)*

Код реализации: (дублировано в [MC.hs](./MC.hs))

```
module MC where

countChange::Int->[Int]->Int
countChange a arr = countChange' a arr where
  countChange' 0 _ = 1
  countChange' _ [] = 0
  countChange' a (c:cs) | c>a = countChange' a cs
                 | otherwise = (countChange' a cs) + (countChange' (a-c) (c:cs))
```

Пример работы:

![IMG1](./img/IMG1.jpg)