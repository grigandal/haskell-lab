module MC where

countChange::Int->[Int]->Int
countChange a arr = countChange' a arr where
  countChange' 0 _ = 1
  countChange' _ [] = 0
  countChange' a (c:cs) | c>a = countChange' a cs
                 | otherwise = (countChange' a cs) + (countChange' (a-c) (c:cs))

