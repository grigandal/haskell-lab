module FA where
  newtype Fun a b = Fun {getFun :: a -> b}

  instance Functor (Fun a) where
    fmap f (Fun b) = Fun (\x -> f (b x))

  instance Applicative (Fun a) where
     pure a = Fun (\_ -> a)
     Fun left <*> Fun right = Fun (\x -> (left x) (right x)) 
