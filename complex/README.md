# Реализация комплексных чисел

Код реализации: (дублировано в [CP.hs](./CP.hs))

```
module CP where

data Complex = Complex {
    realPart :: Float,
    imagPart :: Float
} deriving (Eq, Show)

instance Num Complex where
    (Complex rl il) + (Complex rr ir) = Complex (rl + rr) (il + ir)
    (Complex rl il) - (Complex rr ir) = Complex (rl - rr) (il - ir)
    (Complex rl il) * (Complex rr ir) = Complex (rl * rr - il * ir) (rl * ir + rr * il)
    negate (Complex r i) = Complex (-r) (-i)
    abs (Complex r i) = Complex (sqrt(r*r + i*i)) 0
    signum (Complex r i) = Complex (r / norm) (i / norm) where
        norm = sqrt(r*r + i*i)
```

Пример работы:

![IMG1](./img/IMG1.JPG)