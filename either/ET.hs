module ET where

data Either a b = Left a | Right b

instance Functor (Shit.Either a) where
  fmap f (Shit.Right x) = Shit.Right (f x)
  fmap _ (Shit.Left x) = Shit.Left x

instance Applicative (Shit.Either a) where
  pure = Shit.Right
  Shit.Left  a <*> _ = Shit.Left a
  Shit.Right f <*> right = fmap f right