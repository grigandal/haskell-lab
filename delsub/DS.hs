module DS where

delete :: Char -> String -> String
delete deleted string = [ char | char <- string, char /= deleted ]

substitue :: Char -> Char -> String -> String
substitue deleted added string = [if deleted == char then added else char | char <- string]

substitue2 :: Char -> Char -> String -> String
substitue2 deleted added string = map ((\deleted added char -> if deleted == char then added else char) deleted added) string