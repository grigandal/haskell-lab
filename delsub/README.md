# Реализация функций delete и substitue

Код реализации: (дублировано в [DS.hs](./DS.hs))

```
module DS where

delete :: Char -> String -> String
delete deleted string = [ char | char <- string, char /= deleted ]

substitue :: Char -> Char -> String -> String
substitue deleted added string = [if deleted == char then added else char | char <- string]

substitue2 :: Char -> Char -> String -> String
substitue2 deleted added string = map ((\deleted added char -> if deleted == char then added else char) deleted added) string
```

Пример работы:

![IMG1](./img/IMG1.jpg)