# Проверка балансировки скобок

Код реализации: (дублировано в [BR.hs](./BR.hs))

```
module BR where

balance :: String -> Bool
balance string =
  let
    balance' n [] = if n == 0 then True else False
    balance' n (char:string)
      | char == '(' = balance' (n+1) string
      | char == ')' = if n > 0 then balance' (n-1) string else False
      | otherwise = balance' n string
  in
    balance' 0 string
```

Пример работы:

![IMG1](./img/IMG1.jpg)