# Реализация функций для построения списков треугольных и пирамидальных чисел

Код реализации: (дублировано в [TP.hs](./TP.hs))

```
module TP where

countTriangle::Int->[Int]
countTriangle 1 = [1]
countTriangle x 
	| x == 1 = [1]
	| otherwise = countTriangle (x-1) ++ [x*(x+1) `div` 2]

countPyr::Int->[Int]
countPyr x
    | x == 0 = []
    | otherwise = countPyr (x-1) ++ [sum (countTriangle x)]
```

Пример работы:

![IMG1](./img/IMG1.jpg)