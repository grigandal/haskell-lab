# Реализация функции reverse

Код реализации: (дублировано в [RV.hs](./RV.hs))

```
module RV where

reverse2 :: [a] -> [a]
reverse2 =
  let
    reverse' accum [] = accum
    reverse' accum (x:xs) = reverse' (x:accum) xs
  in
    reverse' []
```

Пример работы:

![IMG1](./img/IMG1.jpg)