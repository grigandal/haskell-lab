module RV where

reverse2 :: [a] -> [a]
reverse2 =
  let
    reverse' accum [] = accum
    reverse' accum (x:xs) = reverse' (x:accum) xs
  in
    reverse' []